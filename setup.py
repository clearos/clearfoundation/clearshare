#!/usr/bin/env python
# -*- encoding: utf-8 -*-
"""
$ python setup.py register sdist upload

First Time register project on pypi
https://pypi.org/manage/projects/

More secure to use twine to upload
$ pip3 install twine
$ python3 setup.py sdist
$ twine upload dist/clearshare-0.0.1.tar.gz

Best practices for setup.py and requirements.txt
https://caremad.io/posts/2013/07/setup-vs-requirement/
"""


from glob import glob
from os.path import basename
from os.path import splitext

from setuptools import setup


setup(
    name='clearshare',
    version='0.0.1',  #  also change in src/clearshare/__init__.py
    license='AGPL3',
    description='ClearSHARE Decentralized Storage',
    long_description="Disk space rental management using ClearSHARE",
    author='',
    author_email='',
    url='https://gitlab.com/clearos/clearfoundation/clearshare',
    packages=['clearshare'],
    py_modules=[splitext(basename(path))[0] for path in glob('clearshare/*.py')],
    package_data={'clearshare': ['clearsia']},
    include_package_data=True,
    zip_safe=False,
    classifiers=[
        # complete classifier list: http://pypi.python.org/pypi?%3Aaction=list_classifiers
        'Development Status :: 3 - Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: Apache Software License',
        'Operating System :: Unix',
        'Operating System :: POSIX',
        'Operating System :: Microsoft :: Windows',
        'Programming Language :: Python :: 3.9',
        'Programming Language :: Python :: Implementation :: CPython',
        # uncomment if you test on these interpreters:
        #'Programming Language :: Python :: Implementation :: PyPy',
        # 'Programming Language :: Python :: Implementation :: IronPython',
        # 'Programming Language :: Python :: Implementation :: Jython',
        # 'Programming Language :: Python :: Implementation :: Stackless',
        'Topic :: Utilities',
    ],
    python_requires='>=3.8.6',
    install_requires=[
        'pysodium==0.7.9',
        'termcolor==1.1.0',
        'cython==0.29.23',
        'falcon==3.0.1',
        'uvicorn==0.14.0',
        'aries_cloudagent==0.6.0',
        'clearlife==0.3.2'
    ],
    setup_requires=[
    ],
    entry_points={
        'console_scripts': [
            'clearshare = clearshare.cli:main',
            'clearshared = clearshare.daemon:main',
        ]
    },
)

