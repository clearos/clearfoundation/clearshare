# ClearSHARE Decentralized Storage

This ClearOS Server application allows users to share excess
available disk space with others in the world. Some important
features of ClearSHARE are:

1. It interfaces with ClearLIFE Server to get derived keys for
   any decentralized identifiers/key pairs. This provides state
   of the art key management for the app using a ClearPHONE and
   its ClearLIFE Mobile application.
2. It abstracts the details of the underlying technology away 
   from the user. This means that switching to other/different
   underlying storage networks is easy later without impacting
   the user experience.
3. Payment can be made using CLEAR instead of the native asset
   of the underlying storage network.

## Quickstart

To interact with the package directly, use the provided shell
scripts. Each one has a `--help` parameter that gives more
information and examples on how to use it. If you just want to
get going, do:

```
clearshare.sh start
```

# Details of Start Operation

The following steps are executed when the `clearshare.sh start`
command is run:

1. Derive app and wallet keys using `clearlife`.
2. Create the wallet and fund it with collateral.
3. Start the command API server.
4. Send a notification to the user that they need
   to set the initial parameters for hosting.
5. Create the host configuration file using the
   wallet and initial parameters.
6. Start the host daemon.

When ClearSHARE first starts, it uses the `clearlife`
executable (from the ClearLIFE Server package) to request
application keys, and to derive a wallet key pair for the
storage network.

Next, the public key part of that key pair is sent to
a ClearFoundation API so that the wallet can be created
on the storage network, added to the metadata monitoring
server, and funded with sufficient collateral to start
participating.

Once the wallet is created and funded, the script sends
a push notification to the user asking them to choose
initial parameters for their hosting. These parameters
include the pricing of available data, how much of their
disk space to rent out, and whether to prioritize renting
or personal space.

After the parameters have been set and returned to the 
clearshare app that requested them, the script generates
the configuration file for the storage network's host
daemon. This configuration includes the derived key wallet
and other parameters set by the user, or chosen by the script
using algorithms and appropriate default settings.

Finally, the host daemon is started. Upon successful startup,
the user receives one more push notification letting them
know that everything is running as expected.