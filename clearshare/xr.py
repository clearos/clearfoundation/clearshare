"""Exchange rate functions for getting averaged live market
rates for assets.
"""
import asyncio
import logging
from datetime import datetime, timezone
import time
import requests

log = logging.getLogger(__name__)

SIA_NAME = "Siacoin"
"""str: name of the SiaCoin on exchanges.
"""
SIA_SYMBOL = "SC"
"""str: symbol of the SiaCoin on exchanges.
"""
X_API_KEY = "804b973bbf194a41a591a2af0ceb3aa7"
"""str: API key for exchange rate queries.
"""

async def get_messari_rate(symbol, name):
    """Queries the messari.io API to get the current exchange rate for XLM.
    """
    from .app import standard_get
    asset = await standard_get(f"https://data.messari.io/api/v1/assets/{symbol}/metrics",
        f"Querying Messari XR for {name} {symbol}.")

    result, updated = None, None
    if asset is not None:
        data = asset["data"]
        if data['name'] != name or data['symbol'] != symbol:
            log.warning(f"The response from Messari.io is not for {name} {symbol}.")
            return result, updated

        result = asset["data"]["market_data"]["price_usd"]
        try:
            updated = datetime(*(time.strptime(asset["data"]["market_data"]["last_trade_at"], 
                "%Y-%m-%dT%H:%M:%S.%fZ")[0:6]), tzinfo=timezone.utc)
        except ValueError:
            # This happens sometimes when the millisecond is missing from response. Try another
            # format string.
            updated = datetime(*(time.strptime(asset["data"]["market_data"]["last_trade_at"], 
                "%Y-%m-%dT%H:%M:%SZ")[0:6]), tzinfo=timezone.utc)
        log.info(f"Found Messari rate as {result:.5f} at {updated}.")

    return result, updated


async def get_gecko_rate(symbol, name):
    """Queries the Gecko Coin API for current exchange rate for XLM.
    """
    # {
    # "{name}": {
    #     "usd": 0.133203,
    #     "last_updated_at": 1609560744
    # }
    # }
    from .app import standard_get
    key = name.lower()
    url = f"https://api.coingecko.com/api/v3/simple/price?ids={key}&vs_currencies=usd&include_last_updated_at=true"
    asset = await standard_get(url, f"Querying Gecko XR for {name} {symbol}.")

    result, updated = None, None
    if asset is not None:
        data = asset[key]
        result, updated = data["usd"], datetime.fromtimestamp(data["last_updated_at"])
        updated = updated.astimezone(timezone.utc)
        log.info(f"Found Gecko rate as {result:.5f} at {updated} for {name} {symbol}.")

    return result, updated


async def get_average_rate(symbol, name):
    """Gets the average price of an asset in USD by querying two different API endpoints.

    Returns:
        float: the average price of Messari and Gecko if both are available; otherwise,
            either one if it is available. `None` if no pricing information is available.
    """
    m, g = await asyncio.gather(
        get_messari_rate(symbol, name), 
        get_gecko_rate(symbol, name)
    )
    messari, mupdated = m
    gecko, gupdated = g

    if messari is not None and gecko is not None:
        # Make sure that the updated times are within 30 seconds of each other.
        delta = abs((mupdated-gupdated).total_seconds())
        log.info(f"Difference between update times for messari and gecko is {delta}.")
        if delta < 300:
            return (messari + gecko)/2.
        else:
            log.warning("Too much time difference between gecko and messari. Using more recent one.")
            if mupdated > gupdated:
                log.info(f"Messari is more up-to-date. Using {messari}.")
                return messari
            else:
                log.info(f"Gecko is more up-to-date. Using {gecko}.")
                return gecko

    elif messari is not None:
        log.warning("Using only messari as source of price; gecko didn't return a result.")
        return messari

    elif gecko is not None:
        log.warning("Using only gecko as source of price; messari didn't return a result.")
        return gecko

    else:
        log.warning("Couldn't get price data from messari or gecko; can't update offer.")
        return None


async def get_sia_rate():
    """Gets the average exchange rate for SC in USD.
    """
    # Use our cached exchange rate instead of querying the APIs directly.
    h = {
        "x-api-key": X_API_KEY
    }
    
    r = requests.get("https://x.clearshare.network/basefund", headers=h)
    d = None
    try:
        d = r.json()
    except:
        log.debug(f"Error deserializing json from sync request {r.text}.")

    log.debug(f"Response from XR request is {d}.")

    if d is not None:
        return d["xr"]