"""General functions for encrypting and decrypting
payloads for communication between ClearPHONE and
this API.
"""
import json, logging
from aries_cloudagent.wallet.util import b58_to_bytes, b64_to_bytes, bytes_to_b64
from clearshare import get_clearlife_client
from datetime import datetime

from .crypto import anon_decrypt_message, anon_crypt_message, verify_signature, sign_message

log = logging.getLogger(__name__)

APP_KEYS = None
"""dict: derived application keys for ClearSHARE.
"""


def get_derived_keys(context, keyid=0):
    """Gets derived application keys, using the base context of the
    application keys.
    """
    life = get_clearlife_client()
    return life.derive(context, keyid)
    

def get_app_keys():
    """Gets application keys, either from ClearLIFE or
    the in-memory cache if available.
    """
    global APP_KEYS
    if APP_KEYS is None:
        life = get_clearlife_client()
        APP_KEYS = life.app_keys

    return APP_KEYS


def decrypt_body(body, target_keys, context=None, decryption_keys=None):
    """Decrypts each of the `target_keys` in `body`
    using the application keys for ClearSHARE.

    Returns:
        dict: copy of `body` where the `target_keys`
            values are overwritten with decrypted values.
    """
    result = body.copy()
    if decryption_keys is None:
        if context is None:
            keys = get_app_keys()
        else:
            keys = get_derived_keys(context)
    else:
        keys = decryption_keys

    for k in target_keys:
        if k not in body:
            continue

        log.debug(f"Decrypting {body[k]} using {keys['public']}.")
        val = anon_decrypt_message(b64_to_bytes(body[k]), 
            b58_to_bytes(keys["public"]),
            bytes.fromhex(keys["secret"]))
        result[k] = val

    return result


def encrypt_body(body, target_keys, app_keys=None):
    """Encrypts each of the `target_keys` in `body`
    using the application keys for ClearSHARE.

    Returns:
        dict: copy of `body` where the `target_keys`
            values are overwritten with encrypted values.
    """
    result = body.copy()
    keys = app_keys if app_keys is not None else get_app_keys()

    for k in target_keys:
        if k not in body:
            continue

        if not isinstance(body[k], str):
            val = bytes_to_b64(anon_crypt_message(json.dumps(body[k]).encode(),
                        b58_to_bytes(keys["public"])))
        else:
            val = bytes_to_b64(anon_crypt_message(b64_to_bytes(body[k]),
                        b58_to_bytes(keys["public"])))
        result[k] = val

    return result


def verify_payload(body):
    """Verifies that the signature matches the message
    in `body`.

    Args:
        body (dict): with keys for `message` and `signature`.
            The signature should be valid with the ClearSHARE
            derived application keys.
    """
    keys = get_app_keys()
    sigok = verify_signature(body["signature"], 
        keys["public"], 
        body["message"])

    if not sigok:
        return False

    # We need to also verify that this is not a replay
    # attack. It has to be within 10 seconds of the 
    # message, which is the int seconds since epoch.
    msgdate = datetime.fromtimestamp(int(body["message"]))
    dateok = (datetime.utcnow() - msgdate).total_seconds() < 10

    return dateok

def verify_decrypt_body(body):
    """Verifies the signature in body and decrypts the `payload` key
    to match the `DidAuthEncryptedRequest` object in Android.
    """
    if not verify_payload(body):
        log.debug(f"The message signature verification failed for {body}.")
        return

    decrypted = {}
    try:
        decrypted = decrypt_body(body, ["payload"])
    except:
        log.debug(f"Decryption failed for {body['payload']}.")
        return

    if "payload" in decrypted:
        return json.loads(decrypted["payload"])


def sign_encrypt_body(body, success=True):
    """Signs and encrypts the body to match the `DidAuthEncryptedEnvelope`
    format expected on ClearLIFE Mobile.

    .. note:: 
    """
    result = {
        "status": body
    }
    keys = get_app_keys()
    result = encrypt_body(result, ["status"], app_keys=keys)
    log.debug(f"Encrypted payload body is {result}.")

    _msg = str(int(datetime.utcnow().timestamp()))
    result.update({
        "success": success,
        "message": _msg,
        "signature": sign_message(_msg.encode(), bytes.fromhex(keys["secret"])).hex()
    })

    return result