"""Module that creates the falcon API application for serving
the `clearlife` API endpoints.
"""
import asyncio
from clearshare.storage import get_storage_status
import falcon
from falcon.asgi import App
import logging
import json
from aiohttp import ClientSession

from .keys import get_app_keys, get_derived_keys, sign_encrypt_body, verify_decrypt_body
from .sia import get_sia_status, get_wallet_status, set_config, get_sia_options, get_sia_account
from .exchange import sell_sc, buy_sc, check_buy_status, check_sell_status

log = logging.getLogger(__name__)


DEFAULT_HOST = "0.0.0.0"
"""str: server name to use by default for binding.
"""
DEFAULT_PORT = 34273
"""int: default port number to bind `clearshare` API to.
"""
SESSION = None
SERVER_HOST = "demo.clearfoundation.com"

app = None

DEBUG = False

async def reset_session():
    """Resets the session object so that it can be recreated by uvicorn.
    """
    global SESSION
    if SESSION is not None:
        await SESSION.close()
    SESSION = None


async def shutdown_app():
    """Cleans up any outstanding application resources, including shared memory.
    """
    log.debug("Shutting down application from main thread.")
    global SESSION
    if SESSION is not None:
        await SESSION.close()


async def standard_get(url, note, headers=None, params=None, agent=True, retried=False):
    """Performs an async GET against the specified URL, handling errors with logging
    and returning `None`; deserialized JSON dictionary returned otherwise.
    """
    global SESSION
    if SESSION is None:
        SESSION = ClientSession()
        log.debug(f"Initialized session {SESSION} from within async context.")

    result = None
    h = {}
    if agent:
        h.update({
            "User-Agent": "Sia-Agent"
        })
    if headers is not None:
        h.update(headers)

    try:
        async with SESSION.get(url, headers=h, params=params) as r:
            if r.status >= 200 and r.status < 300:
                result = await r.json()
            else:
                error = None
                if isinstance(r, str):
                    error = r
                elif hasattr(r, "text"):
                    error = await r.text()
                log.error(f"{note}: {error}")
    except RuntimeError:
        if not retried:
            log.info("Resetting local session and retrying the request.")
            await reset_session()
            return await standard_get(url, note, headers, params, agent, retried=True)
        else:
            log.error(f"While getting {url} after attempted retry.", exc_info=DEBUG)
    except:
        log.error(f"While getting {url}.", exc_info=DEBUG)
    
    return result


async def standard_post(url, body, note, params=None, headers=None, retried=False):
    """Performs an async GET against the specified URL, handling errors with logging
    and returning `None`; deserialized JSON dictionary returned otherwise.
    """
    global SESSION
    if SESSION is None:
        SESSION = ClientSession()
        log.debug(f"Initialized session {SESSION} from within async context.")

    h = {
        "User-Agent": "Sia-Agent"
    }
    if headers is not None:
        h.update(headers)

    result = None
    log.debug(f"POSTing {body} to {url}.")
    try:
        async with SESSION.post(url, headers=h, json=body, params=params) as r:
            if r.status >= 200 and r.status < 300:
                result = await r.json()
                if result is None and r.status == 204:
                    result = {}
            else:
                error = None
                if isinstance(r, str):
                    error = r
                elif hasattr(r, "text"):
                    error = await r.text()
                log.error(f"{note}: {error}")

    except RuntimeError:
        if not retried:
            log.info("Resetting local session and retrying the request.")
            await reset_session()
            return await standard_post(url, body, note, params, headers, retried=True)
        else:
            log.error(f"While getting {url} after attempted retry.", exc_info=DEBUG)
    except:
        log.error(f"While posting to {url}.", exc_info=DEBUG)
    
    return result


def did_auth(req: falcon.Request, context=None, keyid=0):
    """Performs did-based authentication on the request headers.

    Args:
        context (str): key context that the requesting client will be encrypting
            from. If not specified, the base app keys are used.
    """
    from clearlife.auth import auth_did_signature
    if context is not None:
        keys = get_derived_keys(context, keyid)
    else:
        keys = get_app_keys()

    headers, method, url_path = req.headers, req.method, req.path
    ok, _ = auth_did_signature(keys["did"], keys["public"], headers, method, url_path)

    return ok
    

def request_auth(req: falcon.Request, res: falcon.Response, context=None):
    """Checks authentication on the request using DID headers.
    """
    if not did_auth(req, context):
        res.status = falcon.HTTP_UNAUTHORIZED
        res.text = json.dumps({
            "success": False,
            "message": "DID signature/date verification failed for the body."
        })
        return False

    return True


class OptionsResource(object):
    """Endpoint for registering an application with the server.
    """
    async def on_get(self, req: falcon.Request, res: falcon.Response):
        """Gets the current settings/options for the host that are
        configurable by the user.
        """
        if not request_auth(req, res):
            return

        result = sign_encrypt_body(await get_sia_options())
        res.text = json.dumps(result)


    async def on_post(self, req: falcon.Request, res: falcon.Response):
        """Sets new options for the `siad` using `siac`.
        """
        body = await req.media
        if not request_auth(req, res):
            return

        options = verify_decrypt_body(body)
        result = sign_encrypt_body(await set_config(options))
        res.text = json.dumps(result)


class StatusResource(object):
    """Endpoint to retrieve current statistics, including
    balance, contracts, revenue, etc.
    """
    async def on_get(self, req: falcon.Request, res: falcon.Response):
        """Gets a summary of the host status and statistics.
        """
        ok = request_auth(req, res)
        if not ok:
            ok = request_auth(req, res, "host")
        if not ok:
            return

        basic, storage, wallet = await asyncio.gather(
            get_sia_status(),
            get_storage_status(),
            get_wallet_status()
        )
        basic["storage"] = storage
        basic["wallet"] = wallet

        result = sign_encrypt_body(basic)
        res.text = json.dumps(result)


class HealthResource(object):
    """Endpoint to retrieve API health.
    """
    async def on_get(self, req: falcon.Request, res: falcon.Response):
        """Gets a summary of the health of the API server.
        """
        res.text = json.dumps({
            "success": True,
            "status": "healthy"
        })


class BuyResource(object):
    """Endpoint for buying additional SC on the decentralized storage network.
    """
    async def on_get(self, req: falcon.Request, res: falcon.Response):
        """Checks status on a previous `buy` transaction.
        """
        if not request_auth(req, res):
            return

        transaction = req.params.get("transaction")
        if transaction is None:
            res.status = falcon.HTTP_BAD_REQUEST
            res.text = json.dumps({"error": "You must specific a transaction identifier to check."})
            return

        status = await check_buy_status(transaction)
        result = sign_encrypt_body(status)
        res.text = json.dumps(result)


    async def on_post(self, req: falcon.Request, res: falcon.Response):
        """Purchases additional SC by exchanging CLEAR.
        """
        body = await req.media
        if not request_auth(req, res):
            return

        payload = verify_decrypt_body(body)
        result = sign_encrypt_body(await buy_sc(payload))
        res.text = json.dumps(result)


class SellResource(object):
    """Endpoint for cashing out SC for CLEAR.
    """
    async def on_get(self, req: falcon.Request, res: falcon.Response):
        """Checks status on a previous `sell` transaction.
        """
        if not request_auth(req, res):
            return

        transaction = req.params.get("transaction")
        if transaction is None:
            res.status = falcon.HTTP_BAD_REQUEST
            res.text = json.dumps({"error": "You must specific a transaction identifier to check."})
            return

        status = await check_sell_status(transaction)
        result = sign_encrypt_body(status)
        res.text = json.dumps(result)


    async def on_post(self, req: falcon.Request, res: falcon.Response):
        """Triggers the sale of SC for CLEAR.

        Args:
            req : with body that has keys `amount` and `account`. `amount`
                is the SC to transfer; `account` is the target account on
                DEX to put the CLEAR into.
        """
        body = await req.media
        if not request_auth(req, res):
            return

        payload = verify_decrypt_body(body)
        result = sign_encrypt_body(await sell_sc(payload))
        res.text = json.dumps(result)


class AccountResource(object):
    """Endpoint for retrieving the Sia account number to pay CLEAR to.
    """
    async def on_get(self, req: falcon.Request, res: falcon.Response):
        """Gets the primary sia account.
        """
        if not request_auth(req, res):
            return

        status = await get_sia_account()
        result = sign_encrypt_body(status)
        res.text = json.dumps(result)


RESOURCES = {
    "/options": OptionsResource(),
    "/status": StatusResource(),
    "/health": HealthResource(),
    "/buy": BuyResource(),
    "/sell": SellResource(),
    "/account": AccountResource()
}


def configure_server(debug=False):
    """Configures the application for the server.
    """
    global app, DEBUG
    app = App()
    DEBUG = debug

    for route, resource in RESOURCES.items():
        app.add_route(route, resource)