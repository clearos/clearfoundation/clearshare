from clearlife import ClearLifeClient

PACKAGE_NAME = "com.clearos.clearshare"
"""str: fully-qualified package name for key derivation.
"""

def get_clearlife_client():
    """Gets a :class:`ClearLifeClient` instance for
    derived key management.
    """
    return ClearLifeClient(PACKAGE_NAME)