from os import path
import logging
import base64
import re
from pathlib import Path
from itertools import combinations
from operator import itemgetter

from .utility import execute
from .sia import BASE_STORAGE_SECTOR

log = logging.getLogger(__name__)

BASE_MOUNT = "/store"
"""str: base path to which the multidisk is mounted.
"""
BASE_STORAGE = "/var/lib/siad-data"
"""str: path to the primary folder to configure for hosting storage.
"""
API_PASSWORD_FILE = path.join(BASE_STORAGE, "apipassword")
"""str: path to the local file containing the API password.
"""
API_PASSWORD = None
"""str: cached API password."""
AUTH_HEADER = None
"""dict: basic HTTP Authorization header with the API password.
"""
TB = BASE_STORAGE_SECTOR**4
"""int: number of bytes in a terabyte.
"""
CONFIG_DIR_PATTERN = re.compile(r"clearshare-(?P<index>\d+)")
"""re.Pattern: for matching directories in :data:`BASE_STORAGE` that
have been configured using `clearshared`.
"""
CONFIG_DIR_FMT = "clearshare-{0:d}"
"""str: format string to create a directory name compatible with
:data:`CONFIG_DIR_PATTERN`.
"""
MAX_DIR_CAPACITY = int(1*TB)
"""int: maximum number of bytes to allow for any folder that gets
created/configured.
"""


def _get_api_password():
    """Retrieves the API password from file for the host API.
    """
    global API_PASSWORD
    if API_PASSWORD is None:
        if path.isfile(API_PASSWORD_FILE):
            with open(API_PASSWORD_FILE) as f:
                raw_password = f.read().strip()

        basicauth = f":{raw_password}"
        API_PASSWORD = base64.b64encode(basicauth.encode()).decode()

    return API_PASSWORD


def get_auth_header():
    """Returns the header authorization `dict` including the API password.
    """
    global AUTH_HEADER
    if AUTH_HEADER is None:
        AUTH_HEADER = {
            "Authorization": f"Basic {_get_api_password()}",
            "User-Agent": "Sia-Agent"
        }

    return AUTH_HEADER
        

async def get_storage():
    """Requests current storage configuration for the host.

    Returns:
        list: of `dict` folder configuration values; `None` if there is 
            an error or there are no configured folders.
    """
    from .app import standard_get
    url = f"http://localhost:9980/host/storage"
    r = await standard_get(url, "Retrieving existing storage config for host.")
    log.debug(f"Result of storage config request is {r}.")

    if r is not None and r["folders"]:
        return r["folders"]


def get_base_capacity():
    """Retrieves the base capacity of available multi-disk drives that
    can be rented. This uses `df` and returns the *physical* limits 
    for storage rental. :func:`get_storage` returns the capacity and
    used space of *configured* storage.

    Returns:
        dict: with keys `diskCapacity` and `diskUsed` relating to the sum
            of available physical disks from `df`.
    """
    result = {
        "diskCapacity": 0,
        "diskUsed": 0
    }

    x = execute(["df"])
    if len(x["error"]) == 0:
        log.debug(f"Output of `df` is {x['output']}.")
        match = [l.strip() for l in x["output"] if BASE_MOUNT in l]
        log.debug(f"Possible physical disks are {match}.")
        if len(match) > 0 and "/dev/md" in match[0]:
            info = match[0].split()
            log.debug(f"Parsed info line is {info}.")
            used, cap =round(float(info[2])/TB*BASE_STORAGE_SECTOR*4, 0)/4, round(float(info[3])/TB*BASE_STORAGE_SECTOR*4)/4
            result["diskCapacity"] = cap
            result["diskUsed"] = used

    log.debug(f"Discovered {result} for current physical disk limits.")

    return result


async def get_configured_capacity():
    """Analizes the output of :func:`get_storage` to retrieve high-level
    status information about the *configured* capacity.
    """
    c = await get_storage()
    used, cap = 0, 0
    if c is not None:
        for folder in c:
            _capacity = folder["capacity"]
            _left = folder["capacityremaining"]
            cap += _capacity
            used += _capacity - _left

    return {
        "capacity": cap/TB,
        "used": used/TB
    }


async def get_storage_status():
    """Builds the storage sub-status for this host.
    """
    result = get_base_capacity()
    result.update(await get_configured_capacity())

    return result


def _get_existing_os_dirs():
    """Gets a list of the directories that have already been configured
    in Sia using the OS file system.
    """
    p = Path(BASE_STORAGE)
    existing = []
    for c in p.iterdir():
        m = CONFIG_DIR_PATTERN.match(c.name)
        if c.is_dir() and m:
            existing.append((int(m.group("index")), c))

    return existing


def _get_next_config_index(configured=None):
    """Examines the list of already configured folders in the file system
    and returns the next integer to use.

    Args:
        configured (list): of directories that have already been configured.
            If `None`, then :func:`_get_existing_os_dirs` is called to create
            this data.
    """
    result = -1
    if configured is None:
        configured = _get_existing_os_dirs()

    if len(configured) > 0:
        result = max(d[0] for d in configured)

    return result + 1


def _get_next_folder_path(configured=None):
    """Gets the full :class:`pathlib.Path` of the next folder to create
    for expanding storage on this server.

    Args:
        configured (list): of directories that have already been configured.
            If `None`, then :func:`_get_existing_os_dirs` is called to create
            this data.
    """
    p = Path(BASE_STORAGE)
    next_index = _get_next_config_index(configured)
    name = CONFIG_DIR_FMT.format(next_index)

    return p.joinpath(name)


async def resize_single(storage_path, newsize):
    """Submits a request to resize a single storage path. This is intended 
    to only be used for shrinkage operations. Growth operations add folders.

    Returns:
        bool: True if the configuration operation was successful.
    """
    from .app import standard_post
    success = False
    header = get_auth_header()

    params = {
        "path": BASE_STORAGE,
        "newsize": int(newsize*TB)
    }
    log.debug(f"Resizing folder with {params} and {header}.")

    url = "http://localhost:9980/host/storage/folders/resize"
    r = await standard_post(url, None, f"Rezising storage folder to have {newsize} TB.",
        params=params, headers=header)

    if r is not None and "failed" not in r.get("message", ""):
        log.info(f"Resize disk at {storage_path} request appears to have been successful.")
        success = True

    return success


async def remove_single(storage_path, force=False):
    """Submits a request to remove a single storage path.

    Returns:
        bool: True if the configuration operation was successful.
    """
    from .app import standard_post
    success = False
    header = get_auth_header()

    params = {
        "path": BASE_STORAGE,
        "force": force
    }
    log.debug(f"Removing folder {storage_path} with {params} and {header}.")

    url = "http://localhost:9980/host/storage/folders/remove"
    r = await standard_post(url, None, f"Removing storage folder {storage_path}.",
        params=params, headers=header)

    if r is not None and "failed" not in r.get("message", ""):
        log.info(f"Remove disk at {storage_path} request appears to have been successful.")
        success = True

    return success


def _get_folder_combinations(a):
    """Gets a list of all possible combinations and permutations of available
    storage folders so we can get the optimal ones to resize for a shrinkage
    operation.
    """
    possible = []
    for i in range(2, len(a) + 1):
        possible.extend(combinations(range(len(a)), i))

    return possible


def _optimize_shrinkage(a, target):
    """Finds the optimal combination of existing folders to resize for a
    shrinkage operation.
    """
    sizes = []
    for p in range(len(a)):
        s = a[p] - target
        if s >= 0:
            sizes.append(((p,), s, 1))

    for p in _get_folder_combinations(a):
        s = sum(itemgetter(*p)(a)) - target
        if s >= 0:
            sizes.append((p, s, len(p)))

    if len(sizes) > 0:
        candidates = sorted(sizes, key=itemgetter(1, 2))
        optimal = candidates[0][0]
        # Now, sort by the sizes of the candidate combination so that the largest
        # ones are resized first.
        result = sorted(((i, a[i]) for i in optimal), key=itemgetter(1), reverse=True)
    else:
        result = []

    return result


async def _get_folders_to_shrink(shrinkage):
    """Gets a list of the folders that need to be shrunk, giving the largest
    first, and providing the optimal (fewest possible) folders to reach the
    specified shrinkage goal.

    Args:
        target (int): number of bytes to reduce the total disk space by.
    """
    folders = await get_storage()
    available = [f["capacityremaining"] for f in folders]
    optimal = _optimize_shrinkage(available, shrinkage)
    log.info(f"Computed optimal shrinkage folder set as {optimal} from {folders}.")

    if len(optimal) == 0:
        log.warn("There isn't an optimal combination of folders to shrink; "
                    f"shrinking all as much as possible: {folders}")
        return folders

    # optimal output looks like [(0, 750000000000), (3, 250000000000)]
    return [folders[o[0]] for o in optimal]


async def add_single(folder_path, size):
    """Adds a single, new configured directory of `size` bytes for rent
    at `folder_path`.
    """
    from .app import standard_post
    success = False
    header = get_auth_header()

    params = {
        "path": folder_path,
        "size": int(size)
    }
    log.debug(f"Add folder with {params} and {header}.")

    url = "http://localhost:9980/host/storage/folders/add"
    r = await standard_post(url, None, f"Adding first storage folder with {size} TB.",
        params=params, headers=header)

    if r is not None and "failed" not in r.get("message", ""):
        log.info("Add disk request appears to have been successful.")
        success = True

    return success


async def configure_storage(options):
    """Adds/removes/resizes folder options for the storage the 
    host is willing is to rent out.

    Args:
        options (dict): with settings selected by the user.

    Returns:
        bool: True if the configuration operation was successful.
    """
    success = False

    # We have a few different possible paths:
    #
    # 1. No storage is configured yet. In this case, just add the base
    #    folder with the specified amount of storage.
    # 2. The folder(s) exists already and needs to be resized. This has
    #    two possible options: 1) reducing the size; 2) increasing it.
    # 2a) Find the optimal combination of existing folders to reduce the
    #    size for. If there isn't an optimal combination that works, reduce
    #    all of the existing ones as much as possible.
    # 2b) Add a new folder for the increased capacity.
    # 3. The user didn't actually change the capacity of the disk.
    
    # Get current config and see if we have a folder already. The `capacity`
    # option represents the maximum configured rentable space. This is in
    # contrast to `diskCapacity` which is the physical disk limit. The folder
    # size for host rental should be set to `capacity`. Note that the capacity
    # is always specified in TB by the user in the UI.
    xoptions = await get_configured_capacity()
    newsize, xsize = options["capacity"], xoptions["capacity"]
    changed = abs(xsize - newsize) > 0.1

    configured = _get_existing_os_dirs()

    if xoptions["capacity"] < 0.1:
        # This is option 1; nothing has been configured yet.
        next_path = _get_next_folder_path(configured)
        success = await add_single(next_path, int(newsize*TB))
    else:
        # We need to resize the existing disk to be what the user wants.
        # Unless the user didn't actually change the selected size of the
        # disk, in which case, do nothing...
        if changed:
            if newsize < xsize:
                # The user is trying to shrink the available storage. Note
                # that they can only shrink what hasn't been used. We just do
                # the best we can here to reclaim as much as possible.
                reclaim = xsize - newsize
                optimal = await _get_folders_to_shrink(reclaim)
                for folder in optimal:
                    reduced_size = folder["capacity"] - folder["capacityremaining"]
                    folder_path = folder["path"]
                    # See if we should resize or completely remove the configured folder.
                    if folder["capacity"] == folder["capacityremaining"]:
                        # Remove the configured folder.
                        log.info(f"Removing folder {folder_path} with {folder['capacity']} bytes.")
                        ok = await remove_single(folder_path)
                        log.debug(f"Resize for {folder_path} success={ok}")
                    else:
                        log.info(f"Resizing folder {folder_path} to {reduced_size} bytes.")
                        ok = await resize_single(folder_path, reduced_size)
                        log.debug(f"Resize for {folder_path} success={ok}")
                    success = success and ok
            else:
                # They want to expand storage. Create another directory. We want this
                # directory to be at-most 2TB.
                create = newsize - xsize
                while create > 0:
                    next_path = _get_next_folder_path(configured)

                    if create > MAX_DIR_CAPACITY:
                        log.debug("Create max-capacity folder to address remaining capacity "
                                  f"increase of {create} bytes.")
                        ok = await add_single(next_path, MAX_DIR_CAPACITY)
                        create -= MAX_DIR_CAPACITY
                    else:
                        log.debug(f"Creating folder for {create} remaining bytes.")
                        ok = await add_single(next_path, int(create))
                        create = 0

                    success = success and ok

        else:
            log.info(f"{newsize} is not different enough from {xoptions['capacity']}; not resizing.")
            success = True

    return success
