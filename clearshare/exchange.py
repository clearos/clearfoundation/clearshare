"""Functions to exchange CLEAR for SC. These functions leverage
some combination of local transactions + remote call to the APIs
at x.clearshare.network.
"""
from clearshare.utility import utc_now_seconds
from clearshare import get_clearlife_client
from os import lockf, path, remove
from clearlife.crypto import get_signed_headers
import falcon
import json
import logging

from .sia import (is_transaction_confirmed, make_sia_payment, 
                  get_single_transaction, simplify_transaction, filter_simplified)


ETC_CLEAROS_CLEARSHARE="/etc/clearos/clearshare.d"
SIA_RECEIVE_ACCOUNT="3f8acadd14871fc1184ee3ae5de5ccac5bfe54b70a3dc46c85d86da4b5a4f5d94139ba645912"
"""str: 72-digit hex account number for Clear's Sia account where funds should be
sent for exchanging.
"""

log = logging.getLogger(__name__)


def _get_sell_lockfile():
    """Retrieves the lockfile for any existing SC sales that haven't finished
    processing yet.
    """
    return path.join(ETC_CLEAROS_CLEARSHARE, "sell_sc.lock")


def _get_sell_details():
    """Gets the file that stores all transaction details so that we have full 
    source information for exchanges.
    """
    return path.join(ETC_CLEAROS_CLEARSHARE, "sell_details.json")


async def buy_sc(body):
    """Buys additional SC using CLEAR. This is what is triggered
    when the user selects to by more ClearCOIN in the ClearLIFE
    app on ClearPHONE.

    Args:
        body (dict): with keys `amount`, `account` and `transaction` representing
            the amount of CLEAR that ClearLIFE transferred already using
            the `transaction` identifier. This function calls an API on
            the `x.clearshare.network` server to initiate a SC payment
            based on the contents of the DEX transaction and current FMV
            pricing. Also requires `message` and `signature`.
    """
    pass


async def check_buy_status(transaction):
    """Checks the status on a `buy` transaction for SC that was previously
    initiated.

    Args:
        transaction (str): DEX transaction identifier for the incoming payment.
    """
    # Also check the status from the x.clearshare.network endpoint.
    log.debug(f"Checking SC status with x.clearshare.network for transaction {transaction}.")
    status = await _check_x_status(transaction)

    return {
        "success": True,
        "outgoing": status
    }


async def _request_buy_transfer(transaction, amount, target):
    """Requests the transfer of CLEAR in response to a successful sell
    transaction to Clear's SC exchange account.

    Args:
        transaction (str): hash/id of the transaction on the DEX.
        target (str): sia account number to send the SC to.
    """
    from .cli import get_x_url
    url = get_x_url("exchange")
    keys = get_clearlife_client().app_keys
    h = get_signed_headers(keys, url, "POST")

    body = {
        'did': keys["did"],
        'amount': amount,
        'transaction': transaction,
        'pair': "CLEAR-SC"
    }

    from .app import standard_post
    r = await standard_post(url, body, "Request x transfer CLEAR to SC", headers=h)
    log.debug(f"Result of x request CLEAR-SC is {r}.")
    if r is not None:
        return r
    else:
        return {
            "success": False,
            "error": "API/networking error in exchange request.",
            "retry": True
        }


async def _request_sell_transfer(transaction, amount, target):
    """Requests the transfer of CLEAR in response to a successful sell
    transaction to Clear's SC exchange account.

    Args:
        transaction (dict): output of :func:`sia.simplify_transaction` for
            the successful/confirmed transaction.
        target (str): account number on DEX to send the CLEAR to.
    """
    from .cli import get_x_url, get_sia_signature, HOST_CONTEXT
    url = get_x_url("exchange")
    keys = get_clearlife_client().app_keys
    h = get_signed_headers(keys, url, "POST")

    source = transaction["sources"][0]["account"]
    keys = get_clearlife_client().derive(HOST_CONTEXT, 0)
    sig = get_sia_signature(keys, source, target)

    body = {
        'did': keys["did"],
        'amount': amount,
        'transaction': transaction["id"],
        'pair': "SC-CLEAR"
    }
    body.update(sig)

    from .app import standard_post
    r = await standard_post(url, body, "Request x transfer SC to CLEAR", headers=h)
    log.debug(f"Result of x request SC-CLEAR is {r}.")
    if r is not None:
        return r
    else:
        return {
            "success": False,
            "error": "API/networking error in exchange request.",
            "retry": True
        }


async def sell_sc(body):
    """Sells SC to redeem a balance in CLEAR. This is what happens
    when the user chooses "Transfer to ClearPAY" in the ClearLIFE
    settings app on the ClearPHONE.

    .. note:: The server API only allows the user to have a single
        pending sell transaction at any given time.

    Args:
        body (dict): with keys `amount` and `account`. The SC payment transaction to
            send funds to Clear's exchange account is handled by this
            server. Once the transaction is completed, it triggers a call
            to the `x.clearshare.network` server to initiate the CLEAR
            transaction.
    """
    for req in ["amount", "account"]:
        if req not in body:
            return {
                "status": falcon.HTTP_BAD_REQUEST,
                "error": f"The {req} for the exchange is missing."
            }

    # 1. Check if we have a pending SC transfer that hasn't been written
    #    to the block yet.
    # 2. Initiate the SC transfer to Clear's account.
    # 3. Start the waiting/holding pattern until the block is mined and
    #    the transaction goes through.
    # 4. Trigger a request to transfer the CLEAR.
    lockfile = _get_sell_lockfile()
    detail_file = _get_sell_details()
    transaction = None
    if path.isfile(lockfile):
        with open(lockfile) as f:
            transaction = f.read().strip()

        log.debug(f"Retrieved existing transaction {transaction} from lockfile.")

    if transaction is not None:
        # Check status on the transaction and see if the block has been
        # written yet. 
        confirmed = await is_transaction_confirmed(transaction)
        log.debug(f"Transaction {transaction} has confirmed status: {confirmed}")
        if confirmed:
            result = await _request_sell_transfer(confirmed, body["amount"], body["account"])
            # result has fields "success", "payment" and "confirmed"
            if result["success"]:
                log.info(f"Complete exchange for {transaction} was successful: {result['payment']}.")
                log.debug(f"Removing lockfile and detail for this transaction at {lockfile} and {detail_file}.")
                remove(lockfile)
                remove(detail_file)
                return {
                    "success": True,
                    "payment": result['payment']
                }
            else:
                # We need to see if this just still in process; or if it failed forever.
                if "error" in result and result.get("retry") != True:
                    log.error(f"Transaction {transaction} with {confirmed} will not succeed.")
                    log.error(result["error"])

                    log.debug(f"Removing lockfile and detail for this transaction at {lockfile} and {detail_file}.")
                    remove(lockfile)
                    remove(detail_file)
                    return {
                        "success": False,
                        "error": result["error"]
                    }
                else:
                    log.warning(f"Transaction completed but not successful: {result}.")
                    return result
        else:
            log.debug(f"Transaction {transaction} is not yet confirmed. Do nothing.")
            return {
                "success": False,
                "error": "SC transfer transaction is not yet completed."
            }

    else:
        # We haven't done this yet; let's initiate a SC payment to Clear's
        # account and store the transaction information in the lockfile.
        amount = body["amount"]
        log.debug(f"Triggering Sia payment to {SIA_RECEIVE_ACCOUNT} for {amount}.")
        txs = await make_sia_payment(SIA_RECEIVE_ACCOUNT, amount)
        log.debug(f"Send SC result has transaction ids {txs}.")

        with open(detail_file, 'w') as f:
            json.dump({"txs": txs}, f)
            log.debug(f"Wrote tx id info to {detail_file}.")

        # Sort through the transactions to find the id that actually results
        # in payment.
        tx = await filter_simplified(txs, SIA_RECEIVE_ACCOUNT, is_recipient=True)
        log.debug(f"Tx filter found {tx} as sub-transaction targeting Clear's account")
        tx_id = None
        if len(tx) > 0:
            tx_id = tx[0]["id"]
            with open(lockfile, 'w') as f:
                f.write(tx_id)

            log.debug(f"Wrote {tx_id} to {lockfile} as current SC sell transaction.")

        return {
            "success": False,
            "error": f"SC transaction {tx_id} is not yet confirmed."
        }


async def _check_x_status(transaction):
    """Checks the status of an exchange with the x.clearshare.network server API.

    Args:
        transaction (str): either a Sia transaction id or a DEX transaction id
            to check status on with the API.
    """
    log.debug(f"Getting exchange status for tx {transaction}.")
    from .cli import get_x_url
    url = get_x_url("exchange")
    keys = get_clearlife_client().app_keys
    h = get_signed_headers(keys, url, "GET")

    from .app import standard_get
    r = await standard_get(url, f"Getting transaction status for {transaction}.", 
        headers=h, params={"txid": transaction})
    log.debug(f"Result of x status for {transaction} is {r}.")

    return r


async def check_sell_status(transaction):
    """Checks the status of any pending sell transaction; or the one with
    the specified transaction identifier.
    
    Args:
        transaction (str): identifier of a transaction to check exchange status
            on. If `None`, then check status on the latest transaction.
    """
    lockfile = _get_sell_lockfile()
    if transaction is None and path.isfile(lockfile):
        with open(lockfile) as f:
            transaction = f.read().strip()

    if transaction is None or transaction == "":
        return {
            "success": False,
            "outgoing": None,
            "error": "Latest transaction does not exist; specific explicit transaction."
        }

    tx = await get_single_transaction(transaction)
    simple = simplify_transaction(tx["transaction"])

    if tx["confirmed"] is not None:
        # Also check the status from the x.clearshare.network endpoint.
        log.debug(f"Transaction {simple} is confirmed; checking CLEAR status with x.clearshare.network.")
        status = await _check_x_status(transaction)
        simple["x"] = status

    return {
        "success": True,
        "outgoing": simple
    }