#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Module that contains the command line app.

Why does this file exist, and why not put this in __main__?

    You might be tempted to import things from __main__ later, but that will cause
    problems: the code will get executed twice:

    - When you run `python -m keridht` python will execute
      ``__main__.py`` as a script. That means there won't be any
      ``clearshare.__main__`` in ``sys.modules``.
    - When you import __main__ it will get executed again (as a module) because
      there's no ``clearshare.__main__`` in ``sys.modules``.

Also see (1) from http://click.pocoo.org/5/setuptools/#setuptools-integration
"""
from functools import partial
from os import path, environ
import argparse
from re import T
import threading
import logging
import logging.config
import signal
import json
from datetime import datetime
import asyncio, aiohttp
from aries_cloudagent.wallet.util import b58_to_bytes

from clearlife.cli import get_default_args as clearlife_args, _get_url as clearlife_url
from clearlife.crypto import get_signed_headers

from clearshare import get_clearlife_client
from clearshare.utility import relpath, execute, get_serial_number, xpath, utc_now_seconds
from clearshare import msg
from clearshare.base import exhandler, bparser
from clearshare.app import DEFAULT_HOST, DEFAULT_PORT, shutdown_app
from clearshare.sia import get_wallet_status, init_wallet, unlock_wallet, get_wallet_address
from clearshare import PACKAGE_NAME
from clearshare.keys import decrypt_body, verify_decrypt_body

# logfile = relpath("logging.conf")

# for handler in logging.root.handlers[:]:
#     logging.root.removeHandler(handler)
# logging.config.fileConfig(logfile, disable_existing_loggers=False)
log = logging.getLogger(__name__)


shutdown = threading.Event()
"""threading.Event: for signaling shutdown has been triggered for the script.
"""
HOST_CONTEXT = "host"
"""str: context string for deriving the host keys.
"""
CLEARSHARE_EXCHANGE_SERVER = "x.clearshare.network"
"""str: host name of the ClearFoundation server API that exchanges coins
from the decentralized storage networks for CLEAR.
"""
DEBUG_ENABLED = False
"""boolean: global variable to keep track of CLI debug arg to it doesn't
need passed around all over the place.
"""


def examples():
    """Prints examples of using the script to the console using colored output.
    """
    script = "ClearSHARE Manager"
    explain = ("This script manages the creation and configuration of "
               "ClearSHARE decentralized storage network hosts.")
    contents = [(("Get the DID and account number for this host."),
                 "clearshare account",
                 "This derives a seed for the host and returns its DID "
                 "and account on the underlying storage network."),
                (("Create the account and fund it with collateral."),
                 "clearshare fund",
                 "The account from the `account` command is derived "
                 "and then submitted to ClearFoundation API for funding "
                 "This step requires ClearLIFE to have a valid "
                 "Own-Your-Node (OYN) code."),
                 (("Check account status and balance."),
                 "clearshare status",
                 "This queries the ClearSHARE API for account balance "
                 "and status information to display on the marketplace "
                 "dashboard.")
                ]
    required = ("OYN code for account creation/funding.")
    output = ("")
    details = ("")
    outputfmt = ("")

    msg.example(script, explain, contents, required, output, outputfmt, details)


script_options = {
    "command": {"help": "Which action to perform with clearlife",
                "choices": ["account", "fund", "status", "progress", "xstatus", "unlock", "oyn"]},
    "--host": {"help": "Specify the host name to bind the API to.", "default": DEFAULT_HOST},
    "--port": {"help": "Specify the port to bind the API to.", "type": int, "default": DEFAULT_PORT},
    "--force": {"help": "For x.clearshare.network requests; don't use local/cached results "
                        "force the API request to remote server.", "action": "store_true"}
}
"""dict: default command-line arguments and their
    :meth:`argparse.ArgumentParser.add_argument` keyword arguments.
"""


def _parser_options():
    """Parses the options and arguments from the command line."""
    pdescr = "ClearSHARE Account Manager"
    parser = argparse.ArgumentParser(parents=[bparser], description=pdescr)
    for arg, options in script_options.items():
        parser.add_argument(arg, **options)

    args = exhandler(examples, parser)
    ll = logging.DEBUG if args["debug"] else logging.INFO
    logging.basicConfig(level=ll)

    global DEBUG_ENABLED
    DEBUG_ENABLED = args["debug"]

    if args is None:
        return

    return args


def get_x_url(urlpath):
    """Formats a request to the x.clearshare.network exchange server URL.
    
    Args:
        urlpath (str): name of the endpoint to get a URL for.
    """
    return f"https://x.clearshare.network/{urlpath}"


def _get_url(args, urlpath):
    """Gets the formated HTTP URL to send requests to.

    Args:
        urlpath (str): name of the endpoint to get a URL for.
    """
    return f"http://{args['host']}:{args['port']}/{urlpath}"


def _get_json(r):
    """Safely attempts to deserialize the JSON from a request object,
    returning an empty `dict` if that fails.
    """
    try:
        return r.json()
    except json.decoder.JSONDecodeError:
        return {}


def get_default_args():
    """Gets the default command-line args dict for use in programmatic
    execution instead of command-line execution.
    """
    return {
        "host": DEFAULT_HOST,
        "port": DEFAULT_PORT,
        "name": PACKAGE_NAME
    }
    

def stop(signal, frame, _loop:asyncio.AbstractEventLoop=None):
    """Cleans up the nodes, event loops, asyncio, etc.
    """
    global shutdown

    if shutdown.is_set():
        # No need to trigger everything a second time.
        return

    if signal is not None:
        msg.okay(f"SIGNAL {signal}: Cleaning up DHT node and asyncio for {frame}")
    shutdown.set()

    # Also clean up API resources that we used (even without running a full API server)
    if _loop.is_running():
        asyncio.run_coroutine_threadsafe(shutdown_app(), _loop)
    else:
        _loop.run_until_complete(shutdown_app())


def configure_shutdown(_loop:asyncio.AbstractEventLoop):
    """Configures shutdown of the application from the main thread.
    """
    thread_name = threading.current_thread().name
    log.debug(f"Configure shutdown called from {thread_name}.")
    if thread_name == "MainThread":
        stopper = partial(stop, _loop=_loop)
        signal.signal(signal.SIGINT, stopper)
        signal.signal(signal.SIGTERM, stopper)
        log.debug(f"SIGINT and SIGTERM wired to shutdown using {stop}.")


async def get_account_keys(args, print=True):
    """Derives the seed/keys for the storage network host.
    """
    life = get_clearlife_client()
    keys = life.derive(HOST_CONTEXT, 0)
    sia_acc = _get_sia_account(keys)
    if sia_acc is not None:
        keys["account"] = sia_acc

    if print:
        print_account(keys)

    return keys


def _get_sia_account(keys):
    """Generates the SIA account number and keys from the derived
    seed keypair.

    Returns:
        str: with the 72 digit hex account number (aka unlock hash).
    """
    if keys is not None:
        seed = b58_to_bytes(keys["public"]).hex()
        acc_args = [xpath("clearsia"), "account", seed]
        log.debug(f"Finding sia account for public key {keys['public']}.")
        acc_out = execute(acc_args, debug=DEBUG_ENABLED)
        account = None
        if len(acc_out["error"]) == 0 and len(acc_out["output"]) > 0:
            account = acc_out["output"][0].strip()
            log.debug(f"Got account {account} from executing {acc_args}")

        return account
    else:
        log.warn("Cannot retrieve sia account; no keys.")


def find_sia_account(keys, account):
    """Determines the index of the Sia account in the derived key chain.

    Args:
        account (str): 72-digit hex account number (aka unlock hash) of
            the account to find an index for.

    Returns:
        int: index of the derived key that produces `account`.
    """
    seed = b58_to_bytes(keys["seed"]).hex()
    acc_args = [xpath("clearsia"), "find", seed, account]
    acc_out = execute(acc_args, debug=DEBUG_ENABLED)
    account = None
    if len(acc_out["error"]) == 0 and len(acc_out["output"]) > 0:
        try:
            account = int(acc_out["output"][0].strip())
        except ValueError:
            log.debug(f"Error parsing account index {account} as int.")
        log.debug(f"Got account index {account} from executing {acc_args}")

    return account


def get_sia_signature(keys, sender=None, target=None, message=None, index=None):
    """Gets a signature using one of the derived keys of the sia primary
    seed.

    Args:
        keys (dict): base keys for the zeroth host context of the ClearSHARE package.

    Returns:
        dict: with keys `account`, `message`, `signature`, `did`.
    """
    result = {
        "did": keys["did"],
        "account": None,
        "message": None,
        "signature": None
    }

    seed = b58_to_bytes(keys["seed"]).hex()
    if index is None:
        index = find_sia_account(keys, sender)

    if message is None and (sender is not None and target is not None):
        now = utc_now_seconds()
        message = f"{now:.2f}|{sender}|{target}"

    result["message"] = message

    args = [xpath("clearsia"), "sign", seed, message, "-i", index]
    sign_out = execute(args, debug=DEBUG_ENABLED)
    if len(sign_out["error"]) == 0 and len(sign_out["output"]) > 0:
        r = json.loads(sign_out["output"][0])
        if r["account"] != sender:
            msg.err("The account for the signature does not match unlock hash:\n"
                    f"{r['account']} vs. {sender}.")

        result.update(r)

    return result


def print_account(keys):
    """Pretty-prints the keys of the derived account to the 
    console.
    """
    msg.std("HOST DAEMON KEYS\n")
    if keys is not None:
        msg.arb(f"DID: {keys['did']}", ["green", "blue"], ": ", False, indent="first")
        msg.arb(f"Account: {keys['account']}", ["green", "blue"], ": ", False, indent="first")
    else:
        msg.arb("DID: <none>", ["green", "blue"], ": ", False, indent="first")
        msg.arb("Account: <none>", ["green", "blue"], ": ", False, indent="first")


async def _oyn_signature(message):
    """Retrieves the OYN signature dictionary which includes
    a time-sensitive message and its signature, using the OYN
    code that ClearLIFE has.
    """
    payload = {
        "name": PACKAGE_NAME,
        "message": message
    }

    from .app import standard_post
    # Be sure to register a new batch of OTP keys *before* requesting
    # the OYN code (which is encrypted with OTP keys)!
    keys = get_clearlife_client().app_keys
    url = clearlife_url(clearlife_args(PACKAGE_NAME), "oyn")
    r = await standard_post(url, payload, "OYN signing request")
    result = None
    if r is not None:
        log.debug(f"OYN sign response is {r}. Decrypting with {keys['public']}.")
        # We need to decrypt the response to actually get hold of the
        # signature. This is a security feature.
        body = decrypt_body(r, ["payload"], HOST_CONTEXT, keys)
        log.debug(f"Decrypted OYN body is {body}.")
        response = json.loads(body["payload"].decode())
        result = response["signature"]
        msg.okay(f"Retrieved {result} as OYN signature.")

    return result


async def _fund_account(keys):
    """Makes the API request to ClearFoundation to fund the initial
    account on Sia.
    """
    url = get_x_url("fund")
    now = utc_now_seconds()
    message = f"{now:.2f}"

    # Get a new account to transfer the funds to.
    account = get_wallet_address()
    if account is None:
        msg.err("Unable to get new address to send funds to.")
        return
    else:
        msg.info(f"Using new address {account} for funding.")

    payload = {
        "serial": get_serial_number(),
        "oynMessage": message, 
        "oynSignature": await _oyn_signature(message),
        "account": account
    }

    from .app import standard_post
    app_keys = get_clearlife_client().app_keys
    headers = get_signed_headers(app_keys, url, "POST")
    return await standard_post(url, payload, f"Funding account {account}.", headers=headers)


async def _get_funding_status():
    """Gets the progress status from x.clearshare.network on funding the
    account with initial collateral.
    """
    url = get_x_url("status")
    params = {
        "serial": get_serial_number(),
    }

    from .app import standard_get
    keys = get_clearlife_client().app_keys
    headers = get_signed_headers(keys, url, "GET")
    return await standard_get(url, f"Getting funding status for {params['serial']}", 
                                headers=headers, params=params)


async def create_account(args):
    """Queries the ClearFoundation API endpoint to create the account
    for the host daemon, and to fund it with collateral.
    """
    keys = await get_account_keys(args, print=False)
    if keys is not None:
        await _fund_account(keys)
    else:
        log.warn(f"Couldn't get account keys from ClearLIFE.")


async def get_account_status(args):
    """Retrieves account status from the ClearSHARE API server.
    """
    url = _get_url(args, "status")
    keys = await get_account_keys(args, print=False)
    headers = get_signed_headers(keys, url, "GET")
    
    from .app import standard_get
    r = await standard_get(url, "Getting host status", headers=headers)
    if r is not None:
        r["payload"] = r["status"]
        status = verify_decrypt_body(r)

        msg.blank()
        for k, subcat in status.items():
            msg.info(k.title())
            msg.blank()
            for item, value in subcat.items():
                vstr = f"{value:.2f}" if isinstance(value, float) else value
                msg.arb(f"  {item.title()}: {vstr}", ["green", "blue"], ": ", False, indent="first")



async def get_progress(args):
    """Retrieves the progress status of initial funding for the account.

    Returns:
        dict: with keys for `code` and `message`. Message is text describing
            the outcome of the status check. `code` is a number 1, 2, or 3:
            0: Wallet has not been funded; it is also not in progress of being funded.
            1: Wallet has been funded.
            2: Wallet funding has been requested; waiting for the block to write and
               the transaction to be confirmed.
            3: There was an error querying API status.
    """
    wallet = await get_wallet_status()
    if wallet["balance"] > 0 and not args["force"]:
        return {
            "code": 1,
            "message": f"Wallet has been funded; balance is {wallet['balance']:.2f} CLEAR."
        }

    funding = await _get_funding_status()
    if funding is None:
        return {
            "code": 3,
            "message": "Funding status is unavailable due to API error."
        }

    # Response for funding status looks like this.
    # status = {
    #     "serial": serial,
    #     "registered": account is not None,
    #     "funded": funded,
    #     "account": account,
    #     "transaction": tx_id,
    #     "confirmed": confirmed
    # }
    if funding["transaction"] is not None and funding["confirmed"] == False:
        return {
            "code": 2,
            "message": f"Funding via transaction {funding['transaction']} is underway."
        }
    elif funding["transaction"] is not None and funding["confirmed"]:
        return {
            "code": 1,
            "message": f"Funding via transaction {funding['transaction']} complete."
        }
    else:
        if funding["funded"] is None:
            return {
                "code": 0,
                "message": "No funding data available; wallet has never been funded."
            }
        else:
            return {
                "code": 2,
                "message": f"Funding is underway in transitory state."
            }


async def get_xstatus(args):
    """Retrieves the extended progress status of initial funding for the account.

    Returns:
        dict: with keys for "serial", "registered", "funded", "account", 
            "transaction", "confirmed"
    """   
    funding = await _get_funding_status()
    if funding is None:
        msg.err("API error retrieving extended status information.")
        return

    for k, v in funding.items():
        if k != "funded":
            msg.arb(f"  {k.title()}: {v}", ["green", "blue"], ": ", False, indent="first")
        else:
            for kf, vf in v.items():
                if kf[0] == '_' or kf == "account":
                    continue
                msg.arb(f"  {kf.title()}: {vf}", ["green", "blue"], ": ", False, indent="first")

    return funding


async def initialize_account(args):
    """Initializes the wallet from derived seed using the sia API.
    """
    keys = await get_account_keys(args, print=False)
    if keys is not None:
        wallet_ok = await init_wallet(keys)
        if not wallet_ok:
            log.error("Couldn't initialize the wallet using a derived key seed.")
            return
        else:
            log.info("Wallet initialized using derived key seed.")
    else:
        log.warn(f"Couldn't get account keys from ClearLIFE.")


async def run_unlock_wallet(args):
    """Unlocks the sia wallet using the derived key seed.
    """
    keys = await get_account_keys(args, print=False)
    ok = False
    if keys is not None:
        try:
            ok = await unlock_wallet(keys)
        except:
            from .app import DEBUG
            log.error("Unlocking wallet", exc_info=DEBUG)

    if ok:
        msg.okay("Wallet unlock successful.")
    else:
        msg.err("Wallet unlock was not successful.")

    return ok


async def get_oyn_cmd(args):
    """Gets an OYN signature from clearlifed for the current time.
    """
    now = utc_now_seconds()
    message = f"{now:.2f}"

    return await _oyn_signature(message)


CMD_UNLOCK = "unlock"
"""str: command for unlocking the wallet.
"""
CMD_ACCOUNT = "account"
"""str: command for initializing the account from a derived key.
"""
CMD_CREATE = "fund"
"""str: command for funding the storage network account.
"""
CMD_STATUS = "status"
"""str: command to get account status and balance information.
"""
CMD_PROGRESS = "progress"
"""str: command to get account funding progress information.
"""
CMD_XSTATUS = "xstatus"
"""str: command to get extended status from x.clearshare.network.
"""
CMD_OYN = "oyn"
"""str: command to get an OYN signature from clearlifed.
"""

TARGETS = {
  CMD_UNLOCK: run_unlock_wallet,
  CMD_ACCOUNT: initialize_account,
  CMD_CREATE: create_account,
  CMD_STATUS: get_account_status,
  CMD_PROGRESS: get_progress,
  CMD_XSTATUS: get_xstatus,
  CMD_OYN: get_oyn_cmd
}
"""dict: keys are one of the available commands; values
are functions for executing that command.
"""

def run(**args):
    """Starts the local KERI DHT node and API server.
    """
    x = None
    if args["command"] in TARGETS:
        x = TARGETS.get(args["command"])

    loop = asyncio.get_event_loop()
    configure_shutdown(loop)
    if x is not None:
        result = loop.run_until_complete(x(args))
        if args["command"] in [CMD_PROGRESS]:
            # Make sure we have a distinct new line to separate the previous output
            # that may have ended up on stdout from this JSON.
            print('\n' + json.dumps(result))

    stop(None, None, _loop=loop)


def main(args=None):
    args = _parser_options() if args is None else args
    run(**args)


if __name__ == '__main__':  # pragma: no cover
    main()