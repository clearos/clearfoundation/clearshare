"""Basic functions to create cryptographic keys, and to encrypt string data.
"""
"""Basic cryptographic functions to aid in key management for the user.
"""
from random import choice
from hashlib import sha256
from urllib.parse import urlparse
from datetime import datetime

import string
import struct
import nacl.bindings
import logging

from aries_cloudagent.wallet.crypto import (create_keypair, bytes_to_b58, validate_seed,
                                            sign_message, verify_signed_message,
                                            random_seed, b58_to_bytes)

log = logging.getLogger(__name__)


def verify_signature(signature, verkey, message=None):
    """Verifies a signature cryptographically.

    Args:
        agent (str): name of the agent whose master key should be used.
        signature (str): hex-encoded signature that should be verified.
        verkey (str): public key to use in verifying the signature.
    """
    s, v = bytes.fromhex(signature), b58_to_bytes(verkey)
    if message is not None:
        if isinstance(message, bytes):
            return verify_signed_message(s + message, v)
        else:
            return verify_signed_message(s + message.encode("UTF-8"), v)
    else:
        return verify_signed_message(s, v)


def anon_crypt_message(message: bytes, pk: bytes) -> bytes:
    """Apply anon_crypt to a binary message.

    Args:
        message: The message to encrypt
        pk: The verkey to encrypt the message for

    Returns:
        The anon encrypted message
    """
    _pk = nacl.bindings.crypto_sign_ed25519_pk_to_curve25519(pk)
    enc_message = nacl.bindings.crypto_box_seal(message, _pk)
    return enc_message


def anon_decrypt_message(enc_message: bytes, pk: bytes, sk: bytes) -> bytes:
    """Apply anon_decrypt to a binary message.

    Args:
        enc_message: The encrypted message
        pk: public key that the message was encrypted for.
        sk: private key that the message was encrypted for.

    Returns:
        The decrypted message
    """
    _pk = nacl.bindings.crypto_sign_ed25519_pk_to_curve25519(pk)
    _sk = nacl.bindings.crypto_sign_ed25519_sk_to_curve25519(sk)

    message = nacl.bindings.crypto_box_seal_open(enc_message, _pk, _sk)
    return message


def create_did(seed: bytes):
    """Creates a DID and verkey from the given seed.

    Args:
        seed (bytes): the secret seed to use in generating the verkey.

    Returns:
        tuple: `(did, verkey)`.
    """
    seed = validate_seed(seed)
    verkey, _ = create_keypair(seed)
    did = bytes_to_b58(verkey[:16])
    return did, bytes_to_b58(verkey)


def hkdf(master: bytes, salt: bytes, context: bytes, key_len: int = 32, num_keys: int = 1):
    """Derive one or more keys from a master secret using the HMAC-based KDF defined in RFC5869_.

    ..note:: HKDF is a key derivation method approved by NIST in `SP 800 56C`__. This code was adapted from
        :mod:`pycryptodome`.

    Args:
     master (byte string):
        The unguessable value used by the KDF to generate the other keys.
        It must be a high-entropy secret, though not necessarily uniform.
        It must not be a password.
     salt (byte string):
        A non-secret, reusable value that strengthens the randomness of the
        extraction step. Ideally, it is as long as the digest size of the chosen hash.
     key_len (integer):
        The length in bytes of every derived key.
     num_keys (integer):
        The number of keys to derive. Every key is :data:`key_len` bytes long.
     context (byte string):
        Optional identifier describing what the keys are used for.

    Return:
        A byte string or a list of byte strings.
    .. _RFC5869: http://tools.ietf.org/html/rfc5869
    .. __: http://csrc.nist.gov/publications/nistpubs/800-56C/SP-800-56C.pdf
    """
    assert num_keys < 255
    if context is None:
        context = b""
    output_len = key_len * num_keys
    prk = sha256(salt+master).digest()

    t, current = [b""],  b""
    n, tlen = 1, 0
    while tlen < output_len:
        m = sha256()
        m.update(prk + current + context + struct.pack('B', n))
        current = m.digest()
        t.append(current)
        tlen += m.digest_size
        n += 1

    derived_output = b"".join(t)
    if num_keys == 1:
        return derived_output[:key_len]
    kol = [derived_output[idx:idx + key_len] for idx in range(0, output_len, key_len)]
    return list(kol[:num_keys])


def otp_gen_key(text):
    """Generates a random encryption key that is the same length as `text`.

    Args:
        text (str): text that you want to encrypt using OTP.
    """
    return "".join(choice(string.printable) for _ in text)


def otp_encrypt(text, key):
    """Encrypts `text` using the given OTP `key`.

    Args:
        text (str): text to encrypt using OTP.
        key (str): OTP key to encrypt with; must have the same length as `text`.
    """
    assert len(text) == len(key)
    return "".join(chr(ord(i) ^ ord(j)) for (i, j) in zip(text, key))


def otp_decrypt(ciphertext, key):
    """Decrypts the OTP cipher that was generated with `key`.

    Args:
        ciphertext (str): output of the :func:`otp_encrypt`.
        key (str): same key passed to :func:`otp_encrypt` to perform the
            encryption.
    """
    return otp_encrypt(ciphertext, key)


def derive_keys(base, context, keyid):
    """Derives a new set of keys using the specified based keys and a context.

    Args:
        base (dict): returned by :func:`generate_keys`.
        context (str): unique context within which to derive the keys.
        keyid (int): sequence number for the derived key.
    """
    return hkdf(bytes.fromhex(base["secret"]), f"{base['did']}/{keyid}".encode(),
                context.encode())


def generate_keys_b58(seed):
    """Generates keys using the specified Base58 seed.
    """
    byte_seed = b58_to_bytes(seed)
    pk, sk = create_keypair(byte_seed)
    did, verkey = create_did(byte_seed)
    return {    
        "seed": seed, 
        "did": did, 
        "public": verkey, 
        "secret": sk.hex()
    }    


def generate_keys(seed):
    """Generates keys from a given seed.

    Args:
        seed (str): of `32` characters or less. 32 characters encodes a 128-bit seed. If fewer
            than `32` are specified, it will be front-padded with zeros.
    """
    if isinstance(seed, bytes):
        byte_seed = seed
    else:
        byte_seed = f"{seed:0<32}".encode()
    pk, sk = create_keypair(byte_seed)
    did, verkey = create_did(byte_seed)
    return {    
        "seed": bytes_to_b58(seed) if isinstance(seed, bytes) else seed,
        "did": did, 
        "public": verkey, 
        "secret": sk.hex()
    }    