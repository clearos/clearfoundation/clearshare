"""Background Server Daemon for keridht.
"""
import asyncio
from os import path, kill
import argparse
import signal, threading
import logging
import requests
import logging.config
from datetime import datetime, timedelta
from aries_cloudagent.wallet.util import b58_to_bytes
import uvicorn

from clearshare.utility import relpath, execute, xpath
from clearshare import msg
from clearshare.base import exhandler, bparser
from clearshare.app import DEFAULT_PORT, DEFAULT_HOST, shutdown_app, configure_server

from .sia import unlock_wallet, get_wallet_status


logfile = relpath("logging.conf")

# for handler in logging.root.handlers[:]:
#     logging.root.removeHandler(handler)
# logging.config.fileConfig(logfile, disable_existing_loggers=False)
log = logging.getLogger(__name__)


shutdown = threading.Event()
"""threading.Event: for signaling shutdown has been triggered for the script.
"""
wallet_timer = None
"""threading.Timer: for retrying the wallet unlock if it fails at first.
"""

WALLET_UNLOCK_RETRY_INTERVAL=15
"""int: number of seconds to wait between wallet unlock retries.
"""
WALLET_UNLOCK_WATCHDOG_INTERVAL=5*60
"""int: number of seconds to wait between wallet unlock watchdog checks.
"""


def examples():
    """Prints examples of using the script to the console using colored output.
    """
    script = "ClearSHARE Server Daemon"
    explain = ("This script starts a local API endpoint for ClearSHARE server "
               "to accept commands and give status to ClearPHONE and the "
               "ClearOS Marketplace Dashboard for ClearSHARE.")
    contents = [(("Run the server API with default configuration."),
                 "clearshared",
                 f"This runs on {DEFAULT_HOST}:{DEFAULT_PORT}.")]
    required = ("Requires libsodium dependencies installed on the server.")
    output = ("")
    details = ("")
    outputfmt = ("")

    msg.example(script, explain, contents, required, output, outputfmt, details)


script_options = {    
    "--host": {"help": "Specify the host name to bind the API to.", "default": DEFAULT_HOST},
    "--port": {"help": "Specify the port to bind the API to.", "type": int, "default": DEFAULT_PORT}
}
"""dict: default command-line arguments and their
    :meth:`argparse.ArgumentParser.add_argument` keyword arguments.
"""


def _parser_options():
    """Parses the options and arguments from the command line."""
    pdescr = "ClearSHARE Server Daemon"
    parser = argparse.ArgumentParser(parents=[bparser], description=pdescr)
    for arg, options in script_options.items():
        parser.add_argument(arg, **options)

    args = exhandler(examples, parser)
    ll = logging.DEBUG if args["debug"] else logging.WARNING
    logging.basicConfig(level=ll)

    if args is None:
        return

    return args


def stop(sig, frame):
    """Cleans up the nodes, event loops, asyncio, etc.
    """
    global shutdown, wallet_timer
    msg.okay(f"SIGNAL {sig}: Cleaning up server and asyncio for {frame}")
    shutdown.set()

    if wallet_timer is not None:
        log.debug("Cancelling wallet timer for unlock wallet.")
        wallet_timer.cancel()

    try:
        kill(wallet_timer.native_id, signal.SIGKILL)
    except:
        log.warn(f"Error trying to kill wallet timer process at {wallet_timer.native_id}", exc_info=True)


def configure_shutdown():
    """Configures shutdown of the application from the main thread.
    """
    thread_name = threading.current_thread().name
    log.debug(f"Configure shutdown called from {thread_name}.")
    if thread_name == "MainThread":
        signal.signal(signal.SIGINT, stop)
        signal.signal(signal.SIGTERM, stop)
        log.debug(f"SIGINT and SIGTERM wired to shutdown using {stop}.")


async def get_sia_seed():
    """Gets the sia seed via Go executable using the derived keys in
    the appropriate context.
    """
    from .cli import get_account_keys
    keys = await get_account_keys(None, print=False)
    if keys is not None:
        seed = b58_to_bytes(keys["seed"]).hex()
        args = [xpath("clearsia"), seed]
        o = execute(args)

        if len(o["error"]) == 0 and len(o["output"]) > 0:
            return o["output"][0].strip()


def _sync_wallet_unlock():
    """Attempts to unlock the Sia wallet using the current event loop.
    """
    try:
        loop = asyncio.get_event_loop()
    except RuntimeError:
        log.debug("No event loop for wallet unlock timer; creating one.")
        policy = asyncio.get_event_loop_policy()
        loop = policy.new_event_loop()
        asyncio.set_event_loop(loop)

    return loop.run_until_complete(_safe_unlock())


def _reset_unlock_timer(duration, is_unlocked):
    """Resets the timer to trigger a wallet unlock check.
    """
    global wallet_timer
    if wallet_timer is not None:
        log.debug("Cancelling existing unlock wallet timer.")
        wallet_timer.cancel()

    wallet_timer = threading.Timer(duration, _sync_wallet_unlock)
    wallet_timer.start()
    fires_at = datetime.utcnow() + timedelta(seconds=duration)

    status_msg = "already unlocked" if is_unlocked else "locked"
    log.info(f"Creating reset unlock wallet timer for {status_msg} wallet for {fires_at}.")


async def _safe_unlock(retry=True):
    """Attempts to unlock the Sia wallet. If it failes, a timer is set
    to attempt the unlock again later.

    Args:
        retry (bool): when True, set a timer to retry if the operation fails.
    """
    status = await get_wallet_status()
    if status["unlocked"]:
        _reset_unlock_timer(WALLET_UNLOCK_WATCHDOG_INTERVAL, status["unlocked"])
        return True

    from .cli import get_account_keys
    from .app import reset_session

    # Reset the async session for this thread and event loop.
    await reset_session()

    keys = await get_account_keys(None, print=False)
    ok = False
    if keys is not None:
        try:
            ok = await unlock_wallet(keys)
        except:
            from .app import DEBUG
            log.error("Unlocking wallet", exc_info=DEBUG)

    _reset_unlock_timer(WALLET_UNLOCK_RETRY_INTERVAL, ok)

    return ok


def run(**args):
    """Starts the local KERI DHT node and API server.
    """
    loop = asyncio.get_event_loop()
    configure_server(args["debug"])

    global wallet_timer
    fires_at = datetime.utcnow() + timedelta(seconds=WALLET_UNLOCK_RETRY_INTERVAL)
    log.info(f"Creating startup unlock wallet timer for {fires_at}.")
    wallet_timer = threading.Timer(WALLET_UNLOCK_RETRY_INTERVAL, _sync_wallet_unlock)
    wallet_timer.start()

    # If we don't do this reset, the uvicorn spawned process' event loop
    # conflicts in our aiohttp ClientSession.
    from .app import reset_session
    loop.run_until_complete(reset_session())

    uvicorn.run("clearshare.app:app", host=args["host"], port=args["port"], log_level="warning")
    loop.run_until_complete(shutdown_app())
    stop(None, None)


def main(args=None):
    args = _parser_options() if args is None else args
    configure_shutdown()
    run(**args)


if __name__ == '__main__':  # pragma: no cover
    main()