"""Functions for interacting with the `siac` command line
tool to change host config settings.
"""
from clearshare.xr import get_sia_rate
from datetime import datetime
import logging
import asyncio
import requests
import re
from asyncio import iscoroutinefunction

from . import msg
from .crypto import b58_to_bytes
from .utility import execute, utc_now_seconds, xpath

log = logging.getLogger(__name__)
rxunits = re.compile(r"(?P<amount>[\d\.]+)\s*(?P<unit>mS|nS|uS|H|KS)")
rxkvp = re.compile(r"\:\s+")

MINS_PER_BLOCK = 10. # minutes / block
"""float: number of minutes it takes on average to write a block.
"""
WEEK_PER_BLOCK = MINS_PER_BLOCK / (60*24*7)
# minutes / hour * hour / day * day / week = minutes / week
# minutes / block * week / minutes = week / block
"""float: number of *weeks* it takes on average to write a block.
"""
DAY_PER_BLOCK = MINS_PER_BLOCK / (60*24)
"""float: number of *days* it takes on average to write a block.
"""
MONTH_PER_BLOCK = DAY_PER_BLOCK / 30
"""float: number of *months* it takes to write a block (assuming
a month has 30 days).
"""
BASE_STORAGE_SECTOR = 1000
"""int: number of bytes in a sector. Defines whether TB means 1024^4
or 1000^4. Sia uses the definition based on 1000.
"""
TB = BASE_STORAGE_SECTOR**4
"""int: number of bytes in a terabyte.
"""

EXTERNAL = {
    "externalCollateral": "collateral", # hastings / byte / block
    "externalMaxCollateral": "maxcollateral", # hastings
    "externalContractPrice": "contractprice", # hastings
    "externalDownloadPrice": "downloadbandwidthprice", # hastings / byte
    "externalStoragePrice": "storageprice", # hastings / byte / block
    "externalUploadPrice": "uploadbandwidthprice", # hastings / byte
    "externalMaxDuration": "maxduration" # blocks
}
"""dict: keys are from the API request body; values
are the corresponding sia field names for externally
defined values in the GET request for the host API.
"""
SUPPORTED = {
    "maxDuration": "maxduration", # blocks
    "enabled": "acceptingcontracts", # bool
    "storagePrice": "minstorageprice", # hastings / byte / block
    "downloadPrice": "mindownloadbandwidthprice", # hastings / byte
    "uploadPrice": "minuploadbandwidthprice", # hastings / byte
    "contractPrice": "mincontractprice", #hastings
    "collateral": "collateral", # hastings / byte / block
    "collateralBudget": "collateralbudget",
    "maxCollateral": "maxcollateral" # hastings
}
"""dict: keys are from the API request body; values
are the corresponding sia parameter names that can
be configured using `siac`.
"""
SC_CLEAR = None
"""float: number of SiaCoin required to purchase a
single CLEAR.
"""
CLEAR_SC = None
"""float: number of CLEAR needed to buy a single
SiaCoin.
"""
LAST_UPDATE = None
"""float: milliseconds since epoch since the last
update of exchange rates between CLEAR and SiaCoin.
"""
UPDATE_FREQ = 300
"""int: number of seconds between exchange rate 
updates.
"""
UNIT_MAP = {
    "mS": 1e-3,
    "uS": 1e-6,
    "nS": 1e-9,
    "H": 1e-24,
    "SC": 1.,
    "KS": 1e3
}
"""dict: keys are SiaCoin units; values are the multipliers
that take a value into vanilla SiaCoins.
"""


async def get_sc_xr():
    """Queries the messari and gecko API endpoints to
    get the current price of SiaCoin in USD.
    """
    sc = await get_sia_rate()
    if sc is not None:
        return 1./sc


async def get_clear_xr():
    """Gets the current exchange rate value of CLEAR
    from exchanges.

    .. note:: Since CLEAR Coin isn't up and running yet, we 
        are using CLEAR Rewards instead.
    """
    return 1.


async def update_xr():
    """Updates the exchange rate values for SiaCoin to CLEAR
    if needed.
    """
    global CLEAR_SC, SC_CLEAR, LAST_UPDATE
    now = datetime.utcnow().timestamp()
    if LAST_UPDATE is None or now - LAST_UPDATE > UPDATE_FREQ:
        # Get the exchange rate between CLEAR and USD, and then
        # SiaCoin and USD. `clear_usd` is the number of CLEAR
        # you buy with 1 USD. `sia_usd` is t number of SC you
        # can buy with 1 USD. So CLEAR/USD / SC/USD = CLEAR/SC
        # using dimensional analysis.
        clear_usd = await get_clear_xr()
        sia_usd = await get_sc_xr()
        if sia_usd is not None:
            CLEAR_SC = clear_usd / sia_usd
            SC_CLEAR = 1/CLEAR_SC
            LAST_UPDATE = now
        else:
            log.warn("Unable to get valid exchange rate for SC.")


async def clear_to_sc(clear):
    """Converts a price in CLEAR to a price in SiaCoin
    using current exchange rate values.
    """
    # Dimensional analysis: CLEAR * SC / CLEAR = SC
    return clear * SC_CLEAR


async def clear_to_hastings(clear):
    """Converts a value in CLEAR to Hastings using the
    current exchange rates.
    """
    return sc_to_hastings(await clear_to_sc(clear))


async def hastings_to_clear(hastings):
    """Converts a value in hastings to CLEAR using the
    current exchange rate.
    """
    sc = int(hastings) / 1e24
    return await sc_to_clear(sc)


def sc_to_hastings(sc):
    """Converts the specified value in SiaCoin into Hastings.
    A SiaCoin is made up of 10^24 Hastings.
    """
    return int(sc*10**24)


async def sc_to_clear(sc):
    """Converts a price in SiaCoin to CLEAR using current
    exchange rate values.
    """
    # Dimensional analysis: SC * CLEAR / SC = CLEAR
    return sc * CLEAR_SC


def _blocks_to_weeks(blocks: int) -> int:
    """Converts a number of blocks into an approximate number of weeks.
    """
    return int(round(blocks*WEEK_PER_BLOCK))


def _weeks_to_blocks(weeks: int) -> int:
    """Converts a number of weeks into an approximate number of blocks.
    """
    return int(round(weeks/WEEK_PER_BLOCK))


async def _clear_to_hbb(clear: float) -> int:
    """Converts a value in CLEAR / TB / Month into hastings / byte / block.
    """
    #   CLEAR  * hastings/CLEAR             HASTINGS
    # --------   --------------    ->      ----------
    # TB Month * byte / TB block / Month   byte block
    hastings = await clear_to_hastings(clear)
    return int(hastings / TB * MONTH_PER_BLOCK)


async def _hbb_to_clear(hbb) -> float:
    """Converts hastings / byte / block to CLEAR / TB / Month.
    """
    base = int(hbb) / MONTH_PER_BLOCK * TB
    return await hastings_to_clear(base)


async def _clear_to_hb(clear: float) -> int:
    """Converts a value in CLEAR / TB into hastings / byte.
    """
    #   CLEAR  * hastings/CLEAR     HASTINGS
    # --------   -------------- -> ----------
    # TB Month *    byte / TB         byte
    hastings = await clear_to_hastings(clear)
    return int(hastings / TB)


async def _hb_to_clear(hb) -> float:
    """Converts hastings / byte / block to CLEAR / TB / Month.
    """
    base = int(hb) * TB
    return await hastings_to_clear(base)


def _check_connectivity(v):
    """Checks the connectivity status based on the string returned
    by sia API.
    """
    log.debug(f"Checking connectivity via {v}.")
    return "connectable" in v


REPORT_SCHEMA = {
    "general": {
        "connected": ("connectabilitystatus", _check_connectivity),
        "version": ("externalsettings:version", None),
        "enabled": ("externalsettings:acceptingcontracts", None),
        "host": ("externalsettings:netaddress", lambda v: v.split()[0]),
        "contractDuration": ("externalsettings:maxduration", _blocks_to_weeks)
    },
    "collateral": {
        "per": ("externalsettings:collateral", _hbb_to_clear),
        "budget": ("internalsettings:collateralbudget", hastings_to_clear),
        "max": ("externalsettings:maxcollateral", hastings_to_clear),
        "locked": ("financialmetrics:lockedstoragecollateral", hastings_to_clear),
        "risked": ("financialmetrics:riskedstoragecollateral", hastings_to_clear),
        "lost": ("financialmetrics:loststoragecollateral", hastings_to_clear)
    },
    "pricing": {
        "contract": ("externalsettings:contractprice", hastings_to_clear),
        "download": ("externalsettings:downloadbandwidthprice", _hb_to_clear),
        "storage": ("externalsettings:storageprice", _hbb_to_clear),
        "upload": ("externalsettings:uploadbandwidthprice", _hb_to_clear)
    },
    "contracts": {
        "count": ("financialmetrics:contractcount", None),
        "feeIncome": ("financialmetrics:contractcompensation", hastings_to_clear),
        "potentialFeeIncome": ("financialmetrics:potentialcontractcompensation", hastings_to_clear),
        "feeExpense": ("financialmetrics:transactionfeeexpenses", hastings_to_clear)
    },
    "revenue": {
        "storage": ("financialmetrics:storagerevenue", hastings_to_clear),
        "potentialStorage": ("financialmetrics:potentialstoragerevenue", hastings_to_clear),
        "download": ("financialmetrics:downloadbandwidthrevenue", hastings_to_clear),
        "potentialDownload": ("financialmetrics:potentialdownloadbandwidthrevenue", hastings_to_clear),
        "upload": ("financialmetrics:uploadbandwidthrevenue", hastings_to_clear),
        "potentialUpload": ("financialmetrics:potentialuploadbandwidthrevenue", hastings_to_clear)
    }        
}
"""dict: keys are categories of status items; values are
`dict` of status keys and values.
"""


async def get_wallet_status():
    """Gets the wallet status/balance for the Sia node.
    """
    # Get latest XR info for CLEAR to SC conversion.
    await update_xr()

    result = {
        "encrypted": True,
        "unlocked": False,
        "balance": 0.,
        "pendingPayments": 0.,
        "pendingIncome": 0.
    }

    from .app import standard_get
    url = f"http://localhost:9980/wallet"
    r = await standard_get(url, "Retrieving wallet status and balance.")
    log.debug(f"Result of wallet status request is {r}.")

    if r is not None and "encrypted" in r:
        result.update({
            "encrypted": r["encrypted"],
            "unlocked": r["unlocked"],
            "balance": await hastings_to_clear(r["confirmedsiacoinbalance"]),
            "pendingPayments": await hastings_to_clear(r["unconfirmedoutgoingsiacoins"]),
            "pendingIncome": await hastings_to_clear(r["unconfirmedincomingsiacoins"]),
        })

    return result


async def _extract_sia_stats(status):
    """Parses and converts the sia host status text
    into a dictionary of stats we consider useful
    that are in the units of CLEAR.
    """
    # Make sure we have recent exchange rate information.
    await update_xr()

    result = {}
    for category, items in REPORT_SCHEMA.items():
        result[category] = {}
        for option, (text, lamb) in items.items():
            subs, value = text.split(':'), status
            for s in subs:
                if value is not None:
                    value = value.get(s)

            if lamb is None or value is None:
                result[category][option] = value
            else:
                if iscoroutinefunction(lamb):
                    result[category][option] = await lamb(value)
                else:
                    result[category][option] = lamb(value)
                    
    return result


async def get_sia_status():
    """Gets the host status from `siac` and parses it
    to return full status information on host revenue
    and performance.
    """
    from .app import standard_get
    url = f"http://localhost:9980/host"
    r = await standard_get(url, "Retrieving detailed host status.")
    log.debug(f"Result of host status request is {r}.")

    if r is not None and "connectabilitystatus" in r:
        return await _extract_sia_stats(r)
    else:
        return {}


def get_sia_seed(keys):
    """Retrieves the sia-compatible seed using the derived app
    keys for ClearSHARE.
    """
    from .cli import DEBUG_ENABLED

    seed = b58_to_bytes(keys["seed"]).hex()
    args = [xpath("clearsia"), seed]
    out = execute(args, debug=DEBUG_ENABLED)

    if len(out["error"]) == 0:
        seed = out["output"][0].strip()
        wcount = len(seed.split())
        if 28 <= wcount <= 29:
            log.info("Retrieved valid Sia seed from base-application seed.")
            return seed
        else:
            log.warn(f"The seed has {wcount} words; something went wrong.")
    else:
        log.error(''.join(out["error"]))

OPTION_SCHEMA = {
    "maxDuration": _weeks_to_blocks,
    "enabled": lambda v: "true" if v else "false",
    "storagePrice": _clear_to_hbb,
    "downloadPrice": _clear_to_hb,
    "uploadPrice": _clear_to_hb,
    "contractPrice": clear_to_hastings,
    "collateral": _clear_to_hbb,
    "collateralBudget": clear_to_hastings,
    "maxCollateral": clear_to_hastings
}
"""dict: keys are fields in the PUT option payload; values
are functions to apply to each option to convert it into the
Sia params format.
"""

async def format_option_value(option, value):
    """Formats the value of an option being set to 
    the format expected by the host API endpoint.
    """
    lamb = OPTION_SCHEMA.get(option)
    if lamb is None or value is None:
        return value
    else:
        if iscoroutinefunction(lamb):
            return await lamb(value)
        else:
            return lamb(value)


async def _options_to_params(options):
    """Converts the options payload from the UI into the params format
    expected by the Sia API endpoint.
    """
    targets = {}
    for option, value in options.items():
        config = SUPPORTED.get(option)
        if config is None:
            continue

        targets[config] = await format_option_value(option, value)

    return targets


async def set_config(options):
    """Updates the configuration parameters for the
    sia host.

    Args:
        options (dict): keys are option names in :data:`SUPPORTED`
            values are the user's selected values for that option.

    Returns:
        dict: with keys `storage` and `options`; values are `bool`
            indicating whether the storage and option changes were
            applied successfully.
    """
    # Make sure we have recent exchange rate information.
    await update_xr()

    # Add/resize folders that are available for the host to rent.
    from .storage import configure_storage, get_auth_header
    success = await configure_storage(options)

    from .app import standard_post
    params = await _options_to_params(options)
    header = get_auth_header()
    log.debug(f"Settings update with {params} and {header}.")

    url = "http://localhost:9980/host"
    r = await standard_post(url, None, "Saving updated host settings", 
        params=params, headers=header)
    return {
        "storage": success,
        "options": r is not None and "failed" not in r.get("message", "")
    }


GET_OPTION_SCHEMA = {
    "enabled": ("externalsettings:acceptingcontracts", None),
    "maxDuration": ("externalsettings:maxduration", _blocks_to_weeks),
    "collateral": ("internalsettings:collateral", _hbb_to_clear),
    "collateralBudget": ("internalsettings:collateralbudget", hastings_to_clear),
    "maxCollateral": ("internalsettings:maxcollateral", hastings_to_clear),
    "contractPrice": ("internalsettings:mincontractprice", hastings_to_clear),
    "downloadPrice": ("internalsettings:mindownloadbandwidthprice", _hb_to_clear),
    "storagePrice": ("internalsettings:minstorageprice", _hbb_to_clear),
    "uploadPrice": ("internalsettings:minuploadbandwidthprice", _hb_to_clear)
}
"""dict: keys are categories of status items; values are
`dict` of status keys and values.
"""

async def _extract_get_options(status):
    """Parses and converts the sia host status text
    into a dictionary of stats we consider useful
    that are in the units of CLEAR.
    """
    # Make sure we have recent exchange rate information.
    await update_xr()

    result = {}
    for option, (text, lamb) in GET_OPTION_SCHEMA.items():
        subs, value = text.split(':'), status
        for s in subs:
            if value is not None:
                value = value.get(s)

        if lamb is None or value is None:
            result[option] = value
        else:
            if iscoroutinefunction(lamb):
                result[option] = await lamb(value)
            else:
                result[option] = lamb(value)

    return result


async def get_sia_options():
    """Gets the host configurable options from API and parses it
    so that the UI can let the user edit existing values.
    """
    from .app import standard_get
    url = f"http://localhost:9980/host"
    r = await standard_get(url, "Retrieving detailed host status.")
    log.debug(f"Result of host status request is {r}.")

    if r is not None and "connectabilitystatus" in r:
        log.info(f"Connectability: {r['connectabilitystatus']}.")
        from .storage import get_storage_status
        result = await _extract_get_options(r)
        result.update(await get_storage_status())
        return result
    else:
        return {}


async def make_sia_payment(account, amount):
    """Sends `amount` SiaCoins to `account`.
    
    Args:
        account (str): public key / account number for the SiaCoin account.
        amount (float): number of SiaCoins to send. Assumed with no units.

    Returns:
        list: of `str` transaction Ids that were used to send the funds; 
            `None` if the transaction failed and the funds were not sent.
    """
    from .app import standard_post

    params = {
        "amount": sc_to_hastings(amount),
        "destination": account,
        "feeIncluded": True
    }

    url = "http://localhost:9980/wallet/siacoins"
    r = await standard_post(url, None, f"paying SiaCoins to {account}", params=params, for_sia=True)

    if r is not None:
        transactions = r.get("transactionids", [])
        log.debug(f"Response from sending {amount} to {account} is {transactions}.")
        return transactions


def get_confirmation_time(tx) -> datetime:
    """Retrieves the time at which the transaction was confirmed.
    """
    try:
        t = tx["confirmationtimestamp"]
        dt = datetime.fromtimestamp(t)
        return dt
    except OverflowError:
        return None


def simplify_transaction(tx):
    """Simplifies the full output of a SiaCoin transaction to extract just the
    source and destination accounts associated with it.
    
    Args:
        tx (dict): output from the `/wallet/transaction/{transaction_id}` endpoint.
    """
    ti = tx["inputs"]
    to = tx["outputs"]
    src = [{
        "parent": _t["parentid"],
        "account": _t["relatedaddress"],
        "amount": int(_t["value"]),
        "is_wallet": _t["walletaddress"]
    } for _t in ti]
    dst = [{
        "id": _t["id"],
        "account": _t["relatedaddress"],
        "amount": int(_t["value"]),
        "is_wallet": _t["walletaddress"]
    } for _t in to]

    return {
        "id": tx["transactionid"],
        "sources": src,
        "targets": dst,
        "confirmed": get_confirmation_time(tx)
    }


async def get_single_transaction(transaction_id):
    """Retrieves a single transaction's details.

    Args:
        transaction_id (str): transaction id to retrieve details for.
    """
    log.debug(f"Retrieving details for tx {transaction_id} from API.")
    from .app import standard_get
    url = f"http://localhost:9980/wallet/transaction/{transaction_id}"
    r = await standard_get(url, f"Retrieving transaction {transaction_id}", for_sia=True)    
    log.debug(f"Result of tx get for {transaction_id} is {r}.")
    return r


async def get_simplified_transactions(tx_ids):
    """Gets a list of simple transaction details in parallel.
    
    Args:
        tx_ids (list): of `str` transaction identifiers.
    """
    log.info(f"Retrieving simplified transactions for {tx_ids}.")
    tasks = [get_single_transaction(txid) for txid in tx_ids]
    results  = await asyncio.gather(*tasks)
    return [simplify_transaction(tx["transaction"]) for tx in results]


async def filter_simplified(tx_ids, account, is_recipient=None, confirmed=None):
    """Filters the transactions to include only those that include account
    as a sender/recipient of funds.

    Args:
        txs (list): of `str` transaction identifiers to filter.
        account (str): account number to filter by.
        is_recipient (bool): if `True` only return those where `account` is
            the recipient of funds. `False` returns only those where `account`
            is the sender. `None` returns either.
        confirmed (bool): if `True`, only return confirmed transactions; if
            `False` only return unconfirmed. If `None` return either.
    """
    txs = await get_simplified_transactions(tx_ids)
    c, u = [], []
    if confirmed or confirmed is None:
        c = [t for t in txs if t.get("confirmed") is not None]
    if not confirmed or confirmed is None:
        u = [t for t in txs if t.get("confirmed") is not None]

    combined = c + u
    log.debug(f"Filtering combined txs {combined} for account {account}; "
              f"is_recipient: {is_recipient}; confirmed: {confirmed}")
    if is_recipient is None:
        return combined
    else:
        if is_recipient:
            return [t for t in combined if any(_s["account"] == account for _s in t["targets"])]
        else:
            return [t for t in combined if any(_s["account"] == account for _s in t["sources"])]


async def get_wallet_height():
    """Gets the height of the unlocked wallet.
    """
    from .app import standard_get
    url = f"http://localhost:9980/wallet"
    r = await standard_get(url, "Getting wallet height", for_sia=True)
    if r is not None:
        return r.get("height")


async def get_transactions():
    """Gets a list of the recent transactions. We start at the height of the wallet
    -1 to get the most recent transactions. This is to facilitate the lookup of 
    transaction sources, etc. and check that the transaction is completed before 
    attempting to trigger an exchange on the DEX.
    """
    from .app import standard_get
    height = await get_wallet_height()
    url = f"http://localhost:9980/wallet/transactions?startheight={height}&endheight=-1"

    r = await standard_get(url, f"Getting current transactions since {height}", for_sia=True)
    result = None
    if r is not None:
        result = {}
        if r["confirmedtransactions"] is not None:
            ct = [simplify_transaction(_t) for _t in r["confirmedtransactions"]]
            result["confirmed"] = {s["id"]: s for s in ct}
        if r["unconfirmedtransactions"] is not None:
            ut = [simplify_transaction(_t) for _t in r["unconfirmedtransactions"]]
            result["unconfirmed"] = {s["id"]: s for s in ut}

    return result


async def is_transaction_confirmed(transaction_id):
    """Checks whether the specified transaction is confirmed. If it is, the
    simplified transaction details are returned. Otherwise, `None` is returned.

    .. note:: If the transaction is not within the most recent blocks, this 
        function will *not* 

    Args:
        transaction_id (str): transaction identifier to check.

    Returns:
        dict: with outputs defined by :func:`simplify_transaction`; `None`
            if the transaction is not confirmed.
    """
    tx = await get_single_transaction(transaction_id)
    if tx is not None:
        return simplify_transaction(tx["transaction"])

    txs = await get_transactions()
    if transaction_id in txs["confirmed"]:
        return txs["confirmed"][transaction_id]


async def filter_transactions(txs, account, is_recipient=None, confirmed=None):
    """Filters the transactions to include only those that include account
    as a sender/recipient of funds.

    Args:
        txs (list): of transaction identifiers to filter.
        account (str): account number to filter by.
        is_recipient (bool): if `True` only return those where `account` is
            the recipient of funds. `False` returns only those where `account`
            is the sender. `None` returns either.
        confirmed (bool): if `True`, only return confirmed transactions; if
            `False` only return unconfirmed. If `None` return either.
    """
    txs = await get_transactions()
    c, u = [], []
    if confirmed or confirmed is None:
        c = [t for k, t in txs["confirmed"] if k in txs]
    if not confirmed or confirmed is None:
        u = [t for k, t in txs["unconfirmed"] if k in txs]

    combined = c + u
    if is_recipient is None:
        return combined
    else:
        if is_recipient:
            return [t for t in combined if any(_s["account"] == account for _s in t["targets"])]
        else:
            return [t for t in combined if any(_s["account"] == account for _s in t["sources"])]


async def init_wallet(keys):
    """Initializes the Sia wallet with a derived key seed.
    """
    seed = get_sia_seed(keys)
    if seed is None:
        log.error("Unable to unlock Sia wallet; cannot get seed.")
        return
    else:
        parts = seed.split()
        log.info(f"Obtained seed: {parts[0]} {parts[1]} ... {parts[-1]}")

    url = "http://localhost:9980/wallet/init/seed"
    params = {
        "seed": seed
    }
    from .storage import get_auth_header
    header = get_auth_header()

    r = requests.post(url, params=params, headers=header)
    status = r.status_code
    if status == 204:
        log.info("Wallet init from seed appears to be successful.")
        return True
    else:
        error = r.json()
        log.error(f"Error restoring wallet from seed {error}.")

    r.close()

    return False


async def get_sia_account():
    """Gets the base sia account number for the zeroth derived key.
    """
    from .cli import find_sia_account, get_account_keys, get_sia_signature
    from .app import standard_get
    url = f"http://localhost:9980/wallet/address"

    r = await standard_get(url, f"Getting new deposit address", for_sia=True)
    error = None
    if r is not None and "address" in r:
        keys = await get_account_keys(None, print=False)
        acc = r["address"]
        if acc is None:
            error = "Couldn't get a new address to pay."
        else:
            now = utc_now_seconds()
            message = f"{now:..2f}"
            find = find_sia_account(keys, acc)
            if find is None:
                error = f"Couldn't find the index of {acc}."
            else:
                sig = get_sia_signature(keys,  message=message, index=find)
                if sig is None:
                    error = "Couldn't get signed proof of account ownership."
                else:
                    sig["success"] = True
    else:
        return {
            "success": False,
            "error": error
        }


async def unlock_wallet(keys):
    """Unlocks the Sia wallet so that contracts can be formed, etc.
    This uses a derived key from ClearLIFE.
    """
    seed = get_sia_seed(keys)
    if seed is None:
        log.error("Unable to unlock Sia wallet; cannot get seed.")
        return
    else:
        parts = seed.split()
        msg.okay(f"Obtained seed: {parts[0]} {parts[1]} ... {parts[-1]}")

    url = "http://localhost:9980/wallet/unlock"
    params = {
        "encryptionpassword": seed
    }

    from .storage import get_auth_header
    h = get_auth_header()

    r = requests.post(url, params=params, headers=h)
    status = r.status_code
    r.close()
    if status == 204:
        log.info("Wallet unlock appears to be successful.")
        return True

    return False


def get_wallet_address():
    """Gets a new wallet address to send funds to.
    """   
    from .storage import get_auth_header
    url = "http://localhost:9980/wallet/address"
    h = get_auth_header()

    r = requests.get(url, headers=h)
    try:
        d = r.json()
        return d.get("address")
    except:
        log.error("While parsing the JSON response for new wallet address.")
    r.close()